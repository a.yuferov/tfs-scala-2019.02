package tfs.naval

import org.scalatest.{FlatSpec, Matchers}

import scala.util.Random

class SolutionTest extends FlatSpec with Matchers {

  "ships".should("be linear").in ({

    val ship = Lesson.input(0)._2
    Solution.validateShip(ship) shouldBe true

    val ship2 = List(
      (1, 1),
      (1, 2),
      (2, 2)
    )
    Solution.validateShip(ship2) shouldBe false
  })

    "empty ship" should " be invalid" in {

      val ship3 = List()
      Solution.validateShip(ship3) shouldBe false
    }

  "one point ship" should "be valid" in {
    val ship4 = List(
      (3, 4)
    )
    Solution.validateShip(ship4) shouldBe false
  }
    /*val ship4 = List(
      (1, 1),
      (1, 2),
      (1, 3)
    )
    Solution.validateShip(ship4) shouldBe false*/

  "self contained ship" should " be invalid" in {
    val ship5 = List(
      (1, 1),
      (1, 2),
      (1, 1)
    )
    Solution.validateShip(ship5) shouldBe false
  }

  "ships" should "be placed ..." in {
    val field = Vector(
      Vector(false, false, false, false, false),
      Vector(false, true, false, false, false),
      Vector(false, true, false, false, false),
      Vector(false, true, false, false, false),
      Vector(false, false, false, false, false)
    )

    val ship = List(
      (1, 1),
      (1, 2)
    )

    Solution.validatePosition(ship, field) shouldBe false
  }


  "string matchers" should "validate strings" in {
    val result = "Hello scala world"

    result should startWith ("Hello")
    result should include ("scala")
    result should endWith ("world")
  }

  "array matchers" should "validate arrays" in {

    List(1, 2, 3, 4, 5) should contain oneOf (7, 9)
    List(1, 2, 3, 4, 5) should contain atLeastOneOf (2, 3, 4)
    List(1, 2, 3, 4, 5) should contain allOf (2, 3, 5)
    List(1, 2, 2, 3, 3, 3) should contain theSameElementsAs Vector(3, 2, 3, 1, 2, 3)
  }

  "be matcher" should "validate expressions" in {
    val result = Random.nextInt()
    result should be <= 10
  }

  "intercept" should "catch the exception" in {

    intercept[ArithmeticException] {
      1 / 0
    }
  }

  "intercept" should " fail if no exception " in {

    intercept[ArithmeticException] {
      0 / 1
    }
  }



}
